# 1


# YOW! 2013 Kevlin Henney - The SOLID Design Principles Deconstructed #YOW 

Title: YOW! 2013 Kevlin Henney - The SOLID Design Principles Deconstructed #YOW 
Keywords: Principles, Patents 

**First Paragraph:**

The video, YOW! 2013 Kevlin Henney - The SOLID Design Principles Deconstructed, aims to educate viewers on how the SOLID principles aren't so solid. 
Kevin Henney does a fantastic job and challenging each SOLID principle. For example, with single responsibility and liskov, we discover that there are multiple interpretations - at least two for each. Additionally, open / closed doesn't make much sense when viewed from a modern SD perspective with a more refined understanding of version control systems, distinction between code that has been published and code that we own and can change. Lastly, interface segregation is a specific application of single responsibility and so is dependency inversion but more so rate of change. Overall, all of Kevlin's points are relevant and by understanding the presentation it's easer to see how these principles have value with the possible exception as Open/Closed.


**Second Paragraph:**

The strengths of this video are how Kevlin goes through each and every SOLID principle, and finds contradictions in them. The most valuable piece at the end of the video is when he states you only start understanding something when you find contradictions in it. The weaknesses of the video is sometimes Kevlin rambles and goes on tangents, but overall it is a great video that allows you to question the SOLID principles yet realize they are still relevant. 

**Notes** 

- Pattern - A contexutal appropriate solution to a problem 
- NOT PRINCIPLES 

- Understand how people learn 

Experts 
Proficient 
Competent 
Advanced Begineer 
Novice 

- The point here is not simply that somebody who has skill in something learns by doing more of the same, but actually learning a fundamentally different way 

- Guidance or advice for the novice tends to be given in absolutes 'always do this' it's never contextual 

- As you reach expert there becomes rules with context 

- SOLID Principles live at the Novice end 

S- Single Responsibility 
O- Open-Closed 
L- Liskov Substitution 
I- Interface Segregation 
D- Dependency Inversion 

- SR states that every object should have a single responsibility and that responsiblity should be entirely encapsulated by the class, all of its services should totally be narrowly aligned with that responsibility 
This term was introduced by Uncle Bob and it cohesion 
- Cohesion is a measure of strength of assosiation of the elements inside a model. A highly cohesive model is a collection of statements and data items that should be treated as a whole because they are so closely related. Any attempt to divide them up would only result in increased coupling and decreased readability  
- Glenn Vanderburg states coupling will set up a tsunami in your build, coupling is neccessary but there is an optimal amount of coupling
- Write programs that do one thing and do it well. Write programs that work together. 
- The hard part is combining little programs to solve bigger problems 
- Software applications do things they're not good at for the same reason companies do things they're not good at: to avoid transaction costs 
- We use informal units of measure, humans say I've had x amount of glasses of wine instead of counting mls
- **there is not binding principle, no way we can say that these have the characteristics of single responsibility**
- <stdlib.h> like java.util is defined by a negative criteria - not very helpful 
- Every class should embody only about 3-5 distinct responsibilities - Grady Booch 
- CRC - cards ... Responsibilities is plural  
- responsibility is a fluid idea, it's down to our point of view of what we define as a single responsibility 
- Gather together those things that change for the same reason and seperate the things that change for different reasons 

- interace segregation 
- different roles, different interfaces 
- Liskov substitution principles - a type hierarchy is composed of subtypes and supertypes 
- Liskov is often interpreted as a guidance for how to organize your class hierarchy 
- Class hierarchy are layered .. not a lot of people know this 
- When you subclass something that class should not have behavior that does not contradict the behvior of the super class 
- Should have Orthagal behavior 
- It's not as useful as people frame it, and Liskov's research was about abstract data types not Encapsulating classes / etc 

- Open/Closed 
- This is a versioning issue - it basically means use version control 
- Not an OOP principle, this is a use the same version 
- Just keep the same version 
- Knowing how to use version control makes open/closed dead 

-Dependency inversion 
- One of the problems with the inversion name is the name is not a discription of a state but it is a description of an action so dependency inversion is the name of a refactoring it is not the name of a thing, it the name to get to a thing 
- What is the criteria by which I might organize and invert somethig
- If the code keeps changing and is highly volatile than seperate it out, move that over there 

- So what we see here is that SOLID principles are not so solid 
- with single responsiblity, and liskov - we discover that there are multiple interpretations - at least two for each 
- open / closed doesn't make much sense when viewed from a modern SD perspective with a more refined understanding of version control systems, distinction between code that has been published and code that we own and can change 
- interface segregation is a specific application of single responsibility  valid but not saying anything new 
- Dependency inversion is just another specific applciation of single responsibility and specifically about rate of change 
- All of these points are relevant and by understanding the presentation it's easer to see how these principles have value with the possible exception as Open/Closed. 
- You only start understanding something when you find contradictions in it 


# Write a paragraph summary of the differences between the code in the book and the code in the provided repo.

**First Paragraph:**

By first glance at the Book's html it is nearly the identical as the forked repo aside from the name of the action 'SelectBeer.do'. So, the name we want the client to use is different. Additionally, in the book for the web.xml file we are using 'java.sun.com/xml/ns/j2ee' instead of 'xmlns.jcp.org/xml/ns/javaee' in the forked repo. As I comb through I noticed that in the book's web.xml the servlet (name and class) is defined while in the forked repo the servlet is located in the BeerSelectV3.java file. I also noticed subtle differences in the way the servlet code is structured. For instance, located in the doPost function (in the book) you can see the request and print out for beer color. In the forked repo doPost function, we get the beer brands, the beer color, and the suggested beer. When you get to the servlet version 2 code in the book, it's once again nearly the same as the forked repo - the only difference being the use of printwriter and an iterator function (in the book) vs the destroy function (in the repo). Lastly, this leads to the difference in the jsp file - you can see that is where the book utilizes the iterator function whereas the forked repo uses a for each loop to simply print the beer attributes. Later in the tutorial, Code for servlet version three is identical. 


**Book Notes**

1 Client makes request for the form.html page

2 Container retrieves the form.html page

3 Container returns the page to the browser, 
Where the user answers the questions on the form

4 Browser sends request data to the container 

5 The container finds the correct servlet based on the URL, and passes the request to the servlet 

6 The servlet calls the BeerExpert for help 

7 The expert class returns an answer, which the servlet adds to the request object.

8 The servlet forwards the request to the JSP.

9 The JSP gets the answer from the request object.

10 The JSP generates a page for the Container.

11 The container returns the page to the happy user.

Developing a Java web app involved container specific rules and requirements of the Servlets
and the JSP specs. 


**Reflection**

Everything went well, although I was not sure how to create a sequence diagram. 


# 2 

<https://gitlab.oit.duke.edu/mrc90/assignment4headsfirstdemo>

# 3 

# Times for tasks and additional notes

* YOW! 2013 Kevlin Henney - The SOLID Design Principles Deconstructed #YOW  - 1 hr 30 mins
* MVC tutorial + Diagrams + Summary - 2 hr
* The rest was worked on w/ Jack 

